## git summary
* Git is a version control software. Sometimes when there are different copies of your codes made and each code is a better version of the previous one.  You tend to loose the track of your latest code. This is where git comes in handy.

**Some features of git are**
* _Repository_ is a box where you keep all your codes.
* You can save your work using _commit_ function
* You can make an entire copy of the repository on your local machine using _clone_. To copy the repository under your name you need to _fork_ it.
* By taking the analogy of a tree, _master_ is considered as the main code or "trunk of the tree" and a _branch_ is considered as different versions of your code.